import java.util.Scanner;
public class Aufgabe2 {

	public static void main(String[] args) {
	
		Scanner myScanner = new Scanner(System.in);
	
		System.out.print("Guten Tag. Wie lautet Ihr Name? ");
		
		String name = myScanner.next();
		
		System.out.print("\nHallo " + name + "! Wie alt bist du? ");
		
		String age = myScanner.next();
		
		System.out.print("____________________\n____________________\n\n\nDu bist " + name + " und bist " + age + " Jahre alt.");
		
		myScanner.close();
}
}