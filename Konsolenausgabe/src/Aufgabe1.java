import java.util.Scanner;
public class Aufgabe1{


 public static void main(String[] args) 
 {


 Scanner myScanner = new Scanner(System.in);

 System.out.print("Bitte geben Sie eine ganze Zahl ein: ");


 int zahl1 = myScanner.nextInt();

 System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");


 int zahl2 = myScanner.nextInt();


 int ergebnisplus = zahl1 + zahl2;
 int ergebnisminus = zahl1 - zahl2;
 int ergebnismal = zahl1 * zahl2;
 int ergebnisgeteilt = zahl1 / zahl2;
 int ergebnisrest = zahl1 % zahl2;

 System.out.print("\n\n\nErgebnis der Addition lautet: ");
 System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnisplus);
 
 System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
 System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnisminus);
 
 System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
 System.out.print(zahl1 + " x " + zahl2 + " = " + ergebnismal);
 
 System.out.print("\n\n\nErgebnis der Division lautet: ");
 System.out.print(zahl1 + " : " + zahl2 + " = " + ergebnisgeteilt);
 
 System.out.print("\n\n\nErgebnis der Restberechnung lautet: ");
 System.out.print(zahl1 + " : " + zahl2 + " Rest = " + ergebnisrest);
 myScanner.close();
}
}